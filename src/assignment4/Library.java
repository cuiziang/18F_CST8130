package assignment4;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.stream.IntStream;

/************************************************************************************************************
 Purpose:  This class will model a dynamically allocated array of Resource objects to represent the resources
 that have been borrowed from a Library
 Author:  Linda Crane and xxxxxxxxxx
 Course: F2018 - CST8130
 Lab Section: xxxxxxxx
 Data members:  resourcesBorrowed: ArrayList<Resource> - the array
 numResources: int - how many resources are currently stored in array (borrowed)
 max: int - maximum number of resources in the array (Optional - could just use array length)
 Methods: default constructor - uses intitialization at declaration of 1000 spaces in array
 initial constructor (int) - initializes array to size in parameter
 toString (): String - contents of array
 inputResource(Scanner, MyDate): boolean - reads a valid data from the Scanner parameter and the date
 in paramter is today's date and returns through the boolean success or not
 itemsOverdue (MyDate): String - displays resources that are overdue
 deleteResource (Scanner, MyDate): - displays a list of resources and prompts user to select which to delete, then deletes it
 *************************************************************************************************************/

public class Library {
    private ArrayList<LinkedList<Resource>> resourcesBorrowed;

    public Library() {
        this(1000);
    }

    public Library(int max) {
        if (max > 0) resourcesBorrowed = new ArrayList<>(max);
        else resourcesBorrowed = new ArrayList<>();
        IntStream.range(0, max).forEach(value -> resourcesBorrowed.add(new LinkedList<>()));
    }

    public String toString() {
        boolean isFirst;
        StringBuilder out = new StringBuilder("\nItems currently borrowed from library are:\n");
        for (int i = 0; i < resourcesBorrowed.size(); i++) {
            isFirst = true;
            if (!resourcesBorrowed.get(i).isEmpty()) {
                out.append(i + 1).append(": [ ");
                for (Resource resource : resourcesBorrowed.get(i)) {
                    if (isFirst) isFirst = false;
                    else out.append(", ");
                    out.append(resource.toString());
                }
                out.append("]\n");
            }
        }
        return out.toString();
    }

    public boolean inputResource(Scanner in, MyDate today) {

        String type = "";
        char choice = 'k';
        while (!(choice == 'D' || choice == 'M' || choice == 'B')) {
            System.out.print("Enter type of resource being borrowed - D for DVD, M for Magazine and B for book:");
            type = in.next();
            type = type.toUpperCase();
            choice = type.charAt(0);
        }

        Resource temp;
        if (choice == 'D') temp = new DVD();
        else if (type.charAt(0) == 'M') temp = new Magazine();
        else temp = new Book();

        temp.inputResource(in, today);

        int indexToAdd = temp.calculateHash() % resourcesBorrowed.size();
        resourcesBorrowed.get(indexToAdd).addFirst(temp);
        return true;

    }

    public String itemsOverDue(MyDate todayDate) {
        StringBuilder out = new StringBuilder("Items currently borrowed from library that are overdue as of " + todayDate.toString() + " are:\n");
        for (int i = 0; i < resourcesBorrowed.size(); i++)
            if (!resourcesBorrowed.get(i).isEmpty())

                for (Resource resource : resourcesBorrowed.get(i)) {
                    if (resource.isOverDue(todayDate)) {
                        out.append(i + 1).append(": ").append(resource.toString());
                    }
                }
        return out.toString();
    }

    public void deleteResource(Scanner in, MyDate today) {

        System.out.println("Enter the title to delete:");
        String titleToDelete = in.next();

        Resource temp = new Resource(titleToDelete);
        int foundIndex = temp.calculateHash() % resourcesBorrowed.size();

        resourcesBorrowed.get(foundIndex).clear();
        System.out.println("Removed resource");

    }

    public void displaySpecific(Scanner in) {
        if (resourcesBorrowed.size() == 0) {
            System.out.println("No resources to view\n");
            return;
        }

        System.out.print("Enter the title to search for: ");
        String title = in.next();

        Resource temp = new Resource(title);
        int foundIndex = temp.calculateHash() % resourcesBorrowed.size();
        LinkedList<Resource> tempLinkedList = resourcesBorrowed.get(foundIndex);

        if (tempLinkedList.size() > 0) {
            tempLinkedList.forEach(resource -> {
                if (resource.isEqual(temp)) System.out.println(resource);
            });
        } else {
            System.out.println("Resource with this title is not found");
        }
    }
}
