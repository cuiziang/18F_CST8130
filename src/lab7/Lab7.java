package lab7;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.TreeMap;

public class Lab7 {

    public static void main(String[] args) {

        TreeMap<String, Integer> dictionary = new TreeMap<>();
        BufferedReader bufferedReader = null;

        Scanner keyboard = new Scanner(System.in);
        String menuChoice = "a";

        while (menuChoice.charAt(0) != '6') {
            System.out.println("Enter 1 to to clear dictionary,");
            System.out.println("2 to add text from keyboard,");
            System.out.println("3 to add text from a file,");
            System.out.println("4 to search for a word count,");
            System.out.println("5 to display number of nodes,");
            System.out.println("6 to quit");

            menuChoice = keyboard.next();
            switch (menuChoice.charAt(0)) {
                case '1':
                    dictionary.clear();
                    break;
                case '2':
                    System.out.println("Enter text you want add to dictionary:");
                    keyboard.nextLine();
                    String text = keyboard.nextLine();
                    Arrays.stream(text.split(" ")).map(String::toLowerCase).map(word -> word.replaceAll("[^A-Za-z]", "")).forEach(word -> dictionary.put(word, dictionary.containsKey(word) ? dictionary.get(word) + 1 : 1));
                    break;
                case '3':
                    System.out.println("Enter name of file to process: ");
                    try {
                        bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(keyboard.next()))));
                        bufferedReader.lines().forEach(line -> Arrays.stream(line.split(" ")).map(String::toLowerCase).map(word -> word.replaceAll("[^A-Za-z]", "")).forEach(word -> dictionary.put(word, dictionary.containsKey(word) ? dictionary.get(word) + 1 : 1)));
                    } catch (FileNotFoundException e) {
                        System.out.println("File Not Found , return to the  menu.");
                    }
                    break;
                case '4':
                    System.out.println("Enter word to search for:");
                    String keyword = keyboard.next();
                    Integer result;
                    if (dictionary.get(keyword) == null) {
                        result = 0;
                    } else {
                        result = dictionary.get(keyword);
                    }
                    System.out.println(keyword + " occurs " + result + " times");
                    break;
                case '5':
                    System.out.println("There are " + dictionary.size() + " nodes");
                    break;
                case '6':
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid choice...");
            }
        }

    }
}
