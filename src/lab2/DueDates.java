package lab2;

import java.util.Scanner;

/************************************************************************************************************
 Purpose:  This class will model a due dates for assessments in a course
 Author:  Linda Crane and Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 Data members:
 Methods:
 Default constructor – default to 10 dates in the array
 Initial constructor – set array to have size of parameter passed in
 inputDue dates(Scanner): boolean – which reads in values for the dates in the array from the Scanner passed in
 addOne() – adds 1 day to all the dates in the array
 toString(): String – returns a string of the members of the array
 *************************************************************************************************************/

public class DueDates {
    private MyDate[] dueDates;

    public DueDates() {
        this.dueDates = new MyDate[10];
        for (int i = 0; i < this.dueDates.length; i++) {
            this.dueDates[i] = new MyDate();
        }
    }

    public DueDates(int max) {
        this.dueDates = new MyDate[max];
        for (int i = 0; i < this.dueDates.length; i++) {
            this.dueDates[i] = new MyDate();
        }
    }

    public boolean inputDueDates(Scanner in) {
        System.out.println("Enter due dates:");
        for (int i = 0; i < this.dueDates.length; i++) {
            System.out.println((i + 1) + ":");
            dueDates[i].inputDate(in);
        }
        return true;
    }

    public void addOne() {
        for (MyDate dueDate : dueDates) {
            dueDate.addOne();
        }
    }

    public String toString() {
        StringBuilder s = new StringBuilder("");
        for (int i = 0; i < dueDates.length; i++) {
            s.append(i + 1).append(": ").append(dueDates[i]).append("\n");
        }
        return s.toString();
    }

    public String toStringr() {
        StringBuilder s = new StringBuilder("");
        for (MyDate dueDate : dueDates) {
            s.append(dueDate.toStringFile()).append("\n");
        }
        return s.toString();
    }
}

