package lab2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/************************************************************************************************************
 Purpose:  This class is the method main for Lab 1
 Author:  Linda Crane and Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 *************************************************************************************************************/


@SuppressWarnings("ALL")
public class Lab2 {

    private static Scanner keyboard = new Scanner(System.in);
    private static DueDates dueDates;
    private static StringBuilder toWrite = new StringBuilder("");

    private static void readFromFile(String inputFileNameOrnull) {

        Scanner inFile;

        File file = new File(inputFileNameOrnull);
        try {
            if (file.exists()) {
                label:
                while (true) {
                    inFile = new Scanner(file);
                    dueDates = new DueDates(inFile.nextInt());
                    while (inFile.hasNext()) {
                        dueDates.inputDueDates(inFile);
                        dueDates.addOne();
                        toWrite.append(dueDates.toStringr());
                    }
                    System.out.println("\nDue Dates with one day added are\nThe due dates are:");

                    System.out.println(dueDates);

                    System.out.print("Do another (y/n)?");
                    String yOrN = keyboard.next();

                    switch (yOrN) {
                        case "y":
                            System.out.println("Enter name of file to import");
                            inputFileNameOrnull = keyboard.next();
                            continue;
                        case "n":
                            writeToFile();
                            break label;
                        default:
                            System.out.println("Invalid input");
                            break;
                    }
                }
            } else {
                throw new IOException();
            }
        } catch (IOException e) {
            System.out.println("Could not open file...." + inputFileNameOrnull + " exiting");
        } catch (NoSuchElementException e) {
            System.out.println("Incomplete file, please check it.... exiting");
        }
    }

    private static void writeToFile() {
        System.out.print("Enter name of file to write to: ");
        FileWriter outFile;
        try {
            outFile = new FileWriter(keyboard.next());
            outFile.write(toWrite.toString());
            outFile.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print("Due date was write to the file.");
    }

    private static void readFromKeyboard() {
        while (true) {
            System.out.println("How many assessments in this - course must be greater than 0?");

            String numberOfAssessments = keyboard.next();

            if (numberOfAssessments.matches("[1-9]+")) {
                dueDates = new DueDates(Integer.parseInt(numberOfAssessments));
            } else if (numberOfAssessments.equals("")) {
                dueDates = new DueDates();
            } else {
                System.out.println("Invalid input");
                continue;
            }

            dueDates.inputDueDates(keyboard);

            System.out.println("\nThe due dates are:");
            System.out.println(dueDates);

            dueDates.addOne();

            System.out.println("Due Dates with one day added are\nThe due dates are:");
            System.out.println(dueDates);
        }
    }

    public static void main(String[] args) {

        System.out.println("Enter name of file to import or the word null to bypass:");

        String fileOrKeyboard = keyboard.next();

        if (!fileOrKeyboard.equals("null")) {
            readFromFile(fileOrKeyboard);
        } else {
            readFromKeyboard();
        }
    }

}
