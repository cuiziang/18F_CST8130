package assignment2;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.IntStream;

/************************************************************************************************************
 Purpose:  This class will maintain the library
 accommodated in this class.
 Author:  Linda Crane and Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 Data members:
 resourcesBorrowed: Resource[]  - a list of all the resources
 numResources: int - current quantity of Resources
 max: int - max number of resources
 Methods:
 default constructor - initialize the filed
 toString (): String - display resource's information with proper format
 inputResource(): prompt to user to type the resource's info
 resourcesOverDue(): display overdue resources
 deleteResource(): prompt to user to delete resource
 search(): implement binary search
 exch(): exchange specific element in ArrayList
 less(): return whether the latter element is less the first one

 reference:
 https://algs4.cs.princeton.edu/
 *************************************************************************************************************/

@SuppressWarnings("Duplicates")
public class Library {

    private ArrayList<Resource> resourcesBorrowed;

    public Library() {
        this.resourcesBorrowed = new ArrayList<>();
    }

    public void inputResource(Scanner scanner, MyDate myDate) {

        System.out.print("Enter type of resource being borrowed - D for DVD, M for Magazine and B for book:");

        Resource resource;

        switch (scanner.next().toLowerCase()) {
            case "d":
                resource = new DVD();
                break;
            case "m":
                resource = new Magazine();
                break;
            case "b":
            default:
                resource = new Book();
        }

        resource.inputResource(scanner, myDate);

        this.resourcesBorrowed.add(resource);

        this.sortByOurselves(this.resourcesBorrowed);
    }

    private void sortByOurselves(ArrayList<Resource> resourcesBorrowed) {

        for (int i = this.resourcesBorrowed.size() - 1; i > 0 && less(resourcesBorrowed.get(i), resourcesBorrowed.get(i - 1)); i--) {
            exch(resourcesBorrowed, i, i - 1);
        }
    }

    private void exch(ArrayList<Resource> resourcesBorrowed, int j, int i) {
        Resource swap = resourcesBorrowed.get(i);
        resourcesBorrowed.set(i, resourcesBorrowed.get(j));
        resourcesBorrowed.set(j, swap);
    }

    private boolean less(Resource v, Resource w) {
        return v.title.toLowerCase().compareTo(w.title.toLowerCase()) < 0;
    }

    public void inputResourceFromFile(Scanner scanner) {
        try {
            System.out.print("Enter name of file to process:");
            scanner = new Scanner(new File(scanner.next()));
        } catch (FileNotFoundException e) {
            System.out.print("File do not exist, return to the menu.\n");
            return;
        }

        while (scanner.hasNext()) {
            Resource resource;
            switch (scanner.next().toLowerCase()) {
                case "d":
                    resource = new DVD();
                    break;
                case "m":
                    resource = new Magazine();
                    break;
                case "b":
                default:
                    resource = new Book();
            }

            resource.inputResourceFromFile(scanner);

            this.resourcesBorrowed.add(resource);

            this.sortByOurselves(this.resourcesBorrowed);
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Items currently borrowed from library are:\n");
        IntStream.range(0, this.resourcesBorrowed.size()).forEach(value -> stringBuilder.append(value + 1).append(": ").append(resourcesBorrowed.get(value)).append("\n"));
        return stringBuilder.toString();
    }

    public String resourcesOverDue(MyDate today) {

        StringBuilder stringBuilder = new StringBuilder("Items currently borrowed from library that are overdue as of" + "  " + today + " are:\n");
        IntStream.range(0, this.resourcesBorrowed.size()).filter(value -> this.resourcesBorrowed.get(value).isOverDue(today)).forEach(value -> stringBuilder.append(value + 1).append(": ").append(this.resourcesBorrowed.get(value)).append("\n"));
        return stringBuilder.toString();
    }

    public void deleteResource(Scanner scanner, MyDate today) {

        if (this.resourcesBorrowed.size() == 0) {
            System.out.println("No resources to delete\n");
            return;
        }

        while (true) {
            try {
                System.out.println("List of resources checked out in the library:");
                IntStream.range(0, this.resourcesBorrowed.size()).forEach(value -> System.out.println("[" + (value + 1) + "]:" + " " + this.resourcesBorrowed.get(value)));
                System.out.println("Which resource to delete:");

                int toDelete = Integer.parseInt(scanner.next());

                if (this.resourcesBorrowed.get(toDelete - 1).isOverDue(today))
                    this.resourcesBorrowed.get(toDelete - 1).displayOverDue();

                this.resourcesBorrowed.remove(toDelete - 1);
                return;
            } catch (Exception e) {
                System.out.println("You did not type number above, please enter again.\n");
            }
        }
    }

    public boolean isEmpty() {
        return this.resourcesBorrowed.size() == 0;
    }

    public Resource get(String title) {
        int i = rank(title);
        if (i < this.resourcesBorrowed.size() && this.resourcesBorrowed.get(i).title.compareTo(title) == 0)
            return this.resourcesBorrowed.get(i);
        return null;
    }

    public int rank(String title) {
        int lo = 0, hi = this.resourcesBorrowed.size();
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            int cmp = title.toLowerCase().compareTo(this.resourcesBorrowed.get(mid).title.toLowerCase());
            if (cmp < 0) hi = mid - 1;
            else if (cmp > 0) lo = mid + 1;
            else return mid;
        }
        return lo;
    }

    public void search(Scanner scanner) {
        if (isEmpty()) {
            System.out.println("No resources to view\n");
            return;
        }
        System.out.print("Enter the title to search for:");
        String q = scanner.next();

        Resource searchResult = get(q);

        if (searchResult != null) System.out.println(searchResult + "\n");
        else System.out.println("Resource with this title is not found");
    }

    public void exportResourceToFile(Scanner scanner) {
        System.out.print("Enter name of file to write to:");
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(scanner.next())));

            this.resourcesBorrowed.forEach(resource -> {
                try {
                    resource.writeToFile(bufferedWriter);
                    bufferedWriter.write("\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
