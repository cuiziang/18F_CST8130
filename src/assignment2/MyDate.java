package assignment2;

import java.util.Scanner;

/************************************************************************************************************
 Purpose:  This class will model a simple date by keeping day, month and year information.   Leap years are NOT
 accommodated in this class.
 Author:  Linda Crane and Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 Data members:  day : int - value between 1 and 31 inclusive (depending on value in month)
 month: int - value between 1 and 12 inclusive
 year: int - positive value
 Methods: default constructor - sets date to Jan 1, 2018
 toString (): String - prints date in year/moht/day format
 inputDate(Scanner): boolean - reads a valid date from the Scanner parameter and returns through
 boolean success or not
 addOne(): void - adds one to the day field and then adjusts month and year as needed.
 *************************************************************************************************************/
@SuppressWarnings("Duplicates")
public class MyDate implements Comparable<MyDate> {
    private int day;
    private int month;
    private int year;


    public MyDate() {
        this(1, 1, 2018);
    }

    public MyDate(int month, int day, int year) {
        this.month = month;
        this.day = day;
        this.year = year;
    }

    public MyDate(MyDate myDate) {
        this.month = myDate.month;
        this.day = myDate.day;
        this.year = myDate.year;
    }

    public String toString() {
        return "" + year + "/" + month + "/" + day;
    }

    public String toStringFile() {
        return month + " " + day + " " + year;
    }

    public boolean inputDate(Scanner in) {
        month = 0;
        day = 0;
        year = 0;
        do {

            System.out.print("Enter month - between 1 and 12: ");
            if (in.hasNextInt()) this.month = in.nextInt();
            else {
                System.out.println("Invalid month input");
                in.next();
            }
        } while (this.month <= 0 || this.month > 12);

        do {

            System.out.print("Enter day - between 1 and 31: ");
            if (in.hasNextInt()) this.day = in.nextInt();
            else {
                System.out.println("Invalid day input");
                in.next();
            }
        }
        while (this.day <= 0 || this.day > 31 || (this.month == 2 && this.day > 29) || (this.day > 30 && (this.month == 9 || this.month == 4 || this.month == 6 || this.month == 11)));

        do {
            System.out.print("Enter year: ");
            if (in.hasNextInt()) this.year = in.nextInt();
            else {
                System.out.println("Invalid day input");
                in.next();
            }
        } while (this.year <= 0);

        return true;
    }

    public void addOne() {
        if (this.day == 31 || (this.month == 2 && this.day == 29) || (this.day == 30 && (this.month == 9 || this.month == 4 || this.month == 6 || this.month == 11))) {
            if (this.month == 12) {
                this.year++;
                this.month = 1;
                this.day = 1;
            } else {
                this.month++;
                this.day = 1;
            }
        } else {
            this.day++;
        }
    }

    @Override
    public int compareTo(MyDate o) {
        if (this.year > o.year) return 1;
        if (this.year < o.year) return -1;
        if (this.month > o.month) return 1;
        if (this.month < o.month) return -1;
        return Integer.compare(this.day, o.day);
    }


    public boolean isEqual(MyDate myDate) {
        return this.compareTo(myDate) == 0;
    }

    boolean isGreaterThan(MyDate myDate) {
        return this.compareTo(myDate) > 0;
    }


}

