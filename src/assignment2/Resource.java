package assignment2;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;

/************************************************************************************************************
 Purpose:  This class will be the base class and maintain the basic info of resources
 accommodated in this class.
 Author:  Ziang Cui
 Studeng Number: 040912151
 Course: F2018 - CST8130
 Lab Section: 311
 Assignment 1
 Date: Oct 3, 2018
 Data members:
 title : String - resource's title
 borrower : String - resource's borrower
 duedate : MyDate - resource's due date
 Methods:
 constructor(): initialize the filed
 inputResource(): prompt to user to type the resource's info
 isOverDue(): determine if the resource is overdue
 displayOverDue(): Display if overdue
 toString(): display resource's information with proper format
 *************************************************************************************************************/

@SuppressWarnings("Duplicates")
public abstract class Resource {
    String title;
    String borrower;
    MyDate duedate;
    float overdueCost;

    public Resource() {
        this.title = "";
        this.borrower = "";
        this.duedate = new MyDate();
        this.overdueCost = 0.0f;
    }

    public boolean inputResource(Scanner scanner, MyDate myDate) {
        try {
            System.out.print("Enter title being borrowed: ");
            this.title = scanner.next();
            System.out.print("Enter borrower name (no spaces):");
            this.borrower = scanner.next();
            this.duedate = new MyDate(myDate);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void inputResourceFromFile(Scanner scanner) {
        try {
            this.title = scanner.next();
            this.borrower = scanner.next();
            this.duedate = new MyDate(Integer.parseInt(scanner.next()), Integer.parseInt(scanner.next()), Integer.parseInt(scanner.next()));
            this.overdueCost = Float.parseFloat(scanner.next());
        } catch (Exception e) {
            System.out.println("The file is not in correct format, please try again.");
        }
    }

    public boolean isOverDue(MyDate today) {
        return today.isGreaterThan(this.duedate);
    }

    public void displayOverDue() {
        System.out.print("This resource is overdue...borrower owes " + "$" + this.overdueCost + "\n");
    }

    @Override
    public String toString() {
        return this.borrower + " has " + this.title + " due on " + this.duedate;
    }

    public abstract void writeToFile(BufferedWriter bufferedWriter) throws IOException;
}
