package assignment2;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.stream.IntStream;

/************************************************************************************************************
 Purpose:  This class will maintain DVD's info
 accommodated in this class.
 Author:  Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 Data members:
 type : String - resource's type
 Methods:
 constructor(): initialize the filed
 inputResource(): prompt to user to type the resource's info
 toString(): display resource's information with proper format
 *************************************************************************************************************/

@SuppressWarnings("Duplicates")
public class DVD extends Resource {

    private String type;

    public DVD() {
        this.overdueCost = 1.0f;
    }

    @Override
    public boolean inputResource(Scanner scanner, MyDate myDate) {
        try {
            super.inputResource(scanner, myDate);
            System.out.print("Enter the type of DVD (no spaces): ");
            this.type = scanner.next();

            IntStream.rangeClosed(1, 3).forEach(value -> this.duedate.addOne());

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void inputResourceFromFile(Scanner scanner) {
        super.inputResourceFromFile(scanner);
        this.type = scanner.next();
    }

    @Override
    public String toString() {
        return "type of DVD: " + this.type + " " + super.toString() + " and if late " + this.overdueCost;
    }

    @Override
    public void writeToFile(BufferedWriter bufferedWriter) throws IOException {
        bufferedWriter.write(String.format("%s %s %s %s %s %s", "d", this.title, this.borrower, this.duedate.toStringFile(), this.overdueCost, this.type));
    }
}
