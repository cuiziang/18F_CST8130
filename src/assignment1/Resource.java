package assignment1;

import java.util.Scanner;

/************************************************************************************************************
 Purpose:  This class will be the base class and maintain the basic info of resources
 accommodated in this class.
 Author:  Ziang Cui
 Studeng Number: 040912151
 Course: F2018 - CST8130
 Lab Section: 311
 Assignment 1
 Date: Oct 3, 2018
 Data members:
 title : String - resource's title
 borrower : String - resource's borrower
 duedate : MyDate - resource's due date
 Methods:
 constructor(): initialize the filed
 inputResource(): prompt to user to type the resource's info
 isOverDue(): determine if the resource is overdue
 displayOverDue(): Display if overdue
 toString(): display resource's information with proper format
 *************************************************************************************************************/

@SuppressWarnings("Duplicates")
public abstract class Resource {
    protected String title;
    protected String borrower;
    protected MyDate duedate;
    protected float overdueCost;

    public Resource() {
        this.title = "";
        this.borrower = "";
        this.duedate = new MyDate();
        this.overdueCost = 0f;
    }

    public boolean inputResource(Scanner scanner, MyDate myDate) {
        try {
            System.out.print("Enter title being borrowed: ");
            this.title = scanner.next();
            System.out.print("Enter borrower name (no spaces):");
            this.borrower = scanner.next();
            this.duedate = new MyDate(myDate);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isOverDue(MyDate today) {
        return today.isGreaterThan(this.duedate);
    }

    public void displayOverDue(MyDate today) {
        if (today.isGreaterThan(this.duedate)) {
            System.out.println("The item has overdue");
        } else {
            System.out.println("The item do not overdue");
        }
    }

    @Override
    public String toString() {
        return this.borrower + " has time due to on " + this.duedate;
    }
}
