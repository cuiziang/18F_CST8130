package assignment1;

import java.util.Scanner;
import java.util.stream.IntStream;

/************************************************************************************************************
 Purpose:  This class will maintain book's info
 accommodated in this class.
 Author:  Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 Data members:
 author : String - resource's author
 Methods:
 constructor(): initialize the filed
 inputResource(): prompt to user to type the resource's info
 toString(): display resource's information with proper format
 *************************************************************************************************************/

@SuppressWarnings("Duplicates")
public class Book extends Resource {

    private String author;

    public Book() {
        this.overdueCost = 2.0f;
    }

    @Override
    public boolean inputResource(Scanner scanner, MyDate myDate) {
        try {
            super.inputResource(scanner, myDate);
            System.out.print("Enter the author name (no spaces):");
            this.author = scanner.next();

            IntStream.rangeClosed(1, 14).forEach(value -> this.duedate.addOne());

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String toString() {
        return "author " + this.author + " " + super.toString() + " and if late " + this.overdueCost;
    }
}
