package assignment1;

import java.util.Scanner;
import java.util.stream.IntStream;

/************************************************************************************************************
 Purpose:  This class will maintain the library
 accommodated in this class.
 Author:  Linda Crane and Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 Data members:
 resourcesBorrowed: Resource[]  - a list of all the resources
 numResources: int - current quantity of Resources
 max: int - max number of resources
 Methods:
 default constructor - initialize the filed
 toString (): String - display resource's information with proper format
 inputResource(): prompt to user to type the resource's info
 resourcesOverDue(): display overdue resources
 deleteResource(): prompt to user to delete resource
 *************************************************************************************************************/

@SuppressWarnings("Duplicates")
public class Library {

    private int max;
    private Resource[] resourcesBorrowed;
    private int numResources;

    public Library() {
//        TODO:
        this.resourcesBorrowed = new Resource[0];
        this.numResources = 0;
        this.max = 10;
    }

    public void inputResource(Scanner scanner, MyDate myDate) {
        if (this.numResources < this.max) {
            Resource[] resources = new Resource[numResources + 1];
            Resource resource;
            System.out.print("Enter type of resource being borrowed - D for DVD, M for Magazine and B for book:");
            switch (scanner.next().toLowerCase()) {
                case "d":
                    resource = new DVD();
                    break;
                case "m":
                    resource = new Magazine();
                    break;
                case "b":
                default:
                    resource = new Book();
            }
            resource.inputResource(scanner, myDate);

            System.arraycopy(this.resourcesBorrowed, 0, resources, 0, numResources);
            resources[numResources] = resource;
            this.resourcesBorrowed = resources;

            this.numResources++;
        } else {
            System.out.println("Array is full, return to main menu.");
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Items currently borrowed from library are:\n");
        IntStream.range(0, numResources).forEach(value -> stringBuilder.append(value + 1).append(": ").append(resourcesBorrowed[value]).append("\n"));
        return stringBuilder.toString();
    }

    public String resourcesOverDue(MyDate today) {

        StringBuilder stringBuilder = new StringBuilder("Items currently borrowed from library that are overdue as of" + "  " + today + " are:\n");
        IntStream.range(0, numResources).filter(value -> this.resourcesBorrowed[value].isOverDue(today)).forEach(value -> stringBuilder.append(value + 1).append(": ").append(resourcesBorrowed[value]).append("\n"));
        return stringBuilder.toString();
    }

    public void deleteResource(Scanner scanner, MyDate today) {
        Resource[] resources = new Resource[numResources + 1];

        if (numResources != 0) {
            System.out.println("List of resources checked out in the library:");
            IntStream.range(0, numResources).forEach(value -> System.out.println("[" + (value + 1) + "]: " + resourcesBorrowed[value]));
            System.out.print("Which resource to delete:");
            while (true) {
                try {
                    int toDelete = Integer.parseInt(scanner.next());

                    if (this.resourcesBorrowed[toDelete - 1].isOverDue(today)) {
                        System.out.println("This resource is overdue - $" + resourcesBorrowed[toDelete - 1].overdueCost + " is " + "owed by borrower.\n");
                    }

                    System.arraycopy(this.resourcesBorrowed, 0, resources, 0, toDelete - 1);
                    System.arraycopy(this.resourcesBorrowed, toDelete, resources, toDelete - 1, numResources - toDelete);
                    this.resourcesBorrowed = resources;

                    this.numResources--;
                    break;
                } catch (Exception e) {
                    System.out.print("You did not type number in the list above, type it again:");
                }
            }
        } else {
            System.out.println("No resources to delete\n");
        }
    }

}
