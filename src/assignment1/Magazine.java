package assignment1;

import java.util.Scanner;
import java.util.stream.IntStream;

/************************************************************************************************************
 Purpose:  This class will maintain book's info
 accommodated in this class.
 Author:  Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 Data members:
 edition : String - resource's edition
 Methods:
 constructor(): initialize the filed
 inputResource(): prompt to user to type the resource's info
 toString(): display resource's information with proper format
 *************************************************************************************************************/

@SuppressWarnings("Duplicates")
public class Magazine extends Resource {

    private MyDate edition;

    Magazine() {
        this.edition = new MyDate();
        this.overdueCost = 1.0f;
    }

    @Override
    public boolean inputResource(Scanner scanner, MyDate myDate) {
        try {
            super.inputResource(scanner, myDate);
            System.out.print("Enter the edition date: ");
            this.edition.inputDate(scanner);

            IntStream.rangeClosed(1, 7).forEach(value -> this.duedate.addOne());

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String toString() {
        return "edition " + this.edition + " " + super.toString() + " and if late " + this.overdueCost;
    }
}
