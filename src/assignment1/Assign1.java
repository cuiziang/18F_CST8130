package assignment1;

import java.util.Scanner;

/************************************************************************************************************
 Purpose:  This class will maintain a menu for the system
 accommodated in this class.
 Author:  Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 Data members:
 todayDate : MyDate - maintain today's date
 Methods:
 main(): interface of the system
 *************************************************************************************************************/

@SuppressWarnings("Duplicates")
public class Assign1 {

    private static MyDate todayDate;


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Library library = new Library();
        todayDate = new MyDate();

        while (true) {
            System.out.print("Enter 1 to add to resources borrowed,\n" + "2 to display overdue items,\n" + "3 to " + "display all resources borrowed, \n" + "4 to delete a resource, \n" + "5 to change today date\n" + "6 to quit:");

            String choice = scanner.next();

            try {
                switch (Integer.parseInt(choice)) {
                    case 1:
                        library.inputResource(scanner, todayDate);
                        System.out.println();
                        break;
                    case 2:
                        System.out.println(library.resourcesOverDue(todayDate));
                        break;
                    case 3:
                        System.out.println(library);
                        break;
                    case 4:
                        library.deleteResource(scanner, todayDate);
                        break;
                    case 5:
                        System.out.println("Enter a new date for today's date");
                        todayDate.inputDate(scanner);
                        System.out.print("\n");
                        break;
                    case 6:
                        System.out.println("Good bye");
                        return;
                    default:
                        System.out.println("You did not type number from 1 to 6, try again.\n");

                }
            } catch (Exception e) {
                System.out.println("Input error, try again.\n");
            }
        }
    }
}
