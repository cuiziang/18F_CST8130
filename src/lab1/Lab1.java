package lab1;

import java.util.Scanner;

/************************************************************************************************************
 Purpose:  This class is the method main for Lab 1
 Author:  Linda Crane and Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 *************************************************************************************************************/


@SuppressWarnings("Duplicates")
public class Lab1 {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        DueDates dueDates;

        while (true) {
            System.out.println("How many assessments in this - course must be greater than 0?");
            String numberOfAssessments = keyboard.next();

            if (numberOfAssessments.matches("[1-9]+")) {
                dueDates = new DueDates(Integer.parseInt(numberOfAssessments));
            } else if (numberOfAssessments.equals("")) {
                dueDates = new DueDates();
            } else {
                System.out.println("Invalid input");
                continue;
            }

            dueDates.inputDueDates(keyboard);

            System.out.println("The due dates are:");
            System.out.println(dueDates);

            dueDates.addOne();

            System.out.println("Due Dates with one day added are\nThe due dates are:");
            System.out.println(dueDates);


            label:
            while (true) {
                String yesOrNo;

                System.out.print("Do another (y/n)?");
                yesOrNo = keyboard.next();

                switch (yesOrNo) {
                    case "y":
                        break label;
                    case "n":
                        return;
                    default:
                        System.out.println("Invalid input");
                        break;
                }
            }

        }
    }
}

