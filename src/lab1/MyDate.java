package lab1;

import java.util.Scanner;

/************************************************************************************************************
 Purpose:  This class will model a simple date by keeping day, month and year information.   Leap years are NOT
 accommodated in this class.
 Author:  Linda Crane and Ziang Cui
 Course: F2018 - CST8130
 Lab Section: 311
 Data members:  day : int - value between 1 and 31 inclusive (depending on value in month)
 month: int - value between 1 and 12 inclusive
 year: int - positive value
 Methods: default constructor - sets date to Jan 1, 2018
 toString (): String - prints date in year/moht/day format
 inputDate(Scanner): boolean - reads a valid date from the Scanner parameter and returns through
 boolean success or not
 addOne(): void - adds one to the day field and then adjusts month and year as needed.
 *************************************************************************************************************/
public class MyDate {
    private int day;
    private int month;
    private int year;

    public MyDate() {
        this.day = 1;
        this.month = 1;
        this.year = 2018;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String toString() {
        return year + "/" + month + "/" + day;
    }

    public String toStringFile() {
        return month + " " + day + " " + year;
    }

    public boolean inputDate(Scanner in) {
        month = 0;
        day = 0;
        year = 0;
        do {

            System.out.print("Enter month - between 1 and 12: ");
            if (in.hasNextInt()) this.month = in.nextInt();
            else {
                System.out.println("Invalid month input");
                in.next();
            }
        } while (this.month <= 0 || this.month > 12);

        do {

            System.out.print("Enter day - between 1 and 31: ");
            if (in.hasNextInt()) this.day = in.nextInt();
            else {
                System.out.println("Invalid day input");
                in.next();
            }
        }
        while (this.day <= 0 || this.day > 31 || (this.month == 2 && this.day > 29) || (this.day > 30 && (this.month == 9 || this.month == 4 || this.month == 6 || this.month == 11)));

        do {
            System.out.print("Enter year: ");
            if (in.hasNextInt()) this.year = in.nextInt();
            else {
                System.out.println("Invalid day input");
                in.next();
            }
        } while (this.year <= 0);

        return true;
    }

    public void addOne() {
        if (this.day == 31 || (this.month == 2 && this.day == 29) || (this.day == 30 && (this.month == 9 || this.month == 4 || this.month == 6 || this.month == 11))) {
            if (this.month == 12) {
                this.year++;
                this.month = 1;
                this.day = 1;
            } else {
                this.month++;
                this.day = 1;
            }
        } else {
            this.day++;
        }
    }


}

