package assignment3;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Purpose:  Record all the student name of one course.
 * accommodated in this class.
 * Author:  Linda Crane and Ziang Cui
 * Course: F2018 - CST8130
 * Lab Section: 300
 */
@SuppressWarnings("Duplicates")
public class Assign3 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        College college = new College(new ArrayList<>(), "Algonquin");

        String menuChoice = "a";

        while (menuChoice.charAt(0) != '6') {
            System.out.println("\nEnter 1 to display the college courses:");
            System.out.println("2 to add a new course:");
            System.out.println("3 to add a block:");
            System.out.println("4 to verify chain:");
            System.out.println("5 to fix a chain: ");
            System.out.println("6 to quit: ");
            menuChoice = keyboard.next();
            switch (menuChoice.charAt(0)) {
                case '1':
                    college.printCoursesGrades();
                    break;
                case '2':
                    college.addCourse(keyboard);
                    break;
                case '3':
                    college.addGradeToCourse(keyboard);
                    break;
                case '4':
                    college.verifyChains();
                    break;
                case '5':
                    college.fixChain(keyboard);
                    break;
                case '6':
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid choice...");
            }
        }
    }
}

