package assignment3;

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

/**
 * Purpose:  Record all the student name of one course.
 * accommodated in this class.
 * Author:  Linda Crane and Ziang Cui
 * Course: F2018 - CST8130
 * Lab Section: 300
 */
public class CourseGrades {
    /**
     * The Linkedlist of all the block.
     */
    private LinkedList<Block> blockChain;
    /**
     * The name of the course.
     */
    private String courseName;

    /**
     * Constructor.
     */
    public CourseGrades(String courseName) {
        this.blockChain = new LinkedList<>();
        this.courseName = courseName;
        this.blockChain.add(new Block());
    }

    /**
     * Return the course name.
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * Print the all score of a course
     */
    public void printBlockChain() {
        System.out.println("\nFor course: " + this.courseName);
        System.out.print("[");
        blockChain.forEach(block -> System.out.print("\n" + block));
        System.out.print("]\n");
    }

    /**
     * Verify whether every score is correct.
     */
    public boolean verifyChain() {
        Block prevBlock = blockChain.getFirst();
        for (Block block : blockChain.subList(1, blockChain.size())) {
            if (!block.isEqual(prevBlock)) {
                return false;
            }
            prevBlock = block;
        }
        return true;
    }

    /**
     * Add a block to blockchain.
     */
    public void addBlock(Scanner keyboard) {
        Block block = new Block();
        if (block.addInfoToBlock(keyboard, blockChain.getLast().getCurrentHash())) blockChain.add(block);
    }

    /**
     * Add a bad block to blockchain.
     */
    public void addBadBlock(Scanner keyboard) {
        Block block = new Block();
        if (block.addInfoToBlock(keyboard, new Random().nextFloat())) blockChain.add(block);
    }

    /**
     * Remove the first broken block in a specific blockchain.
     */
    public void fixChain() {
        Block prevBlock = blockChain.getFirst();
        for (int i = 1; i < this.blockChain.size(); i++) {
            if (this.blockChain.get(i).isEqual(prevBlock)) {
                prevBlock = this.blockChain.get(i);
            } else {
                this.blockChain.remove(this.blockChain.get(i));
                if (i != this.blockChain.size()) this.blockChain.get(i).updatePreviousHash(prevBlock.getCurrentHash());
                break;
            }
        }
        System.out.println("Course is fixed");
    }
}
