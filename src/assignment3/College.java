package assignment3;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Purpose:  Record all the student name of one course.
 * accommodated in this class.
 * Author:  Linda Crane and Ziang Cui
 * Course: F2018 - CST8130
 * Lab Section: 300
 */
public class College {

    /**
     * The ArrayList of all the courses
     */
    private ArrayList<CourseGrades> courseGradesArrayList;
    /**
     * The name of the college
     */
    private String collegeName;

    /**
     * Initialize the filed.
     */
    public College(ArrayList<CourseGrades> courseGradesArrayList, String collegeName) {
        this.courseGradesArrayList = courseGradesArrayList;
        this.collegeName = collegeName;
    }


    /**
     * Add a course from user input
     */
    public void addCourse(Scanner keyboard) {
        System.out.println("Enter name of course to add:");
        this.courseGradesArrayList.add(new CourseGrades(keyboard.next()));
    }

    /**
     * Print all the scores recorded.
     */
    public void printCoursesGrades() {
        System.out.println("For college: " + this.collegeName);
        if (!this.courseGradesArrayList.isEmpty()) {
            courseGradesArrayList.forEach(CourseGrades::printBlockChain);
        }
    }

    /**
     * Add scores to a course from user input
     */
    public void addGradeToCourse(Scanner keyboard) {
        System.out.println("Enter which course to add :");

        IntStream.range(0, this.courseGradesArrayList.size()).forEach(value -> System.out.println(value + " " + courseGradesArrayList.get(value).getCourseName()));

        int choice = (Character.getNumericValue(keyboard.next().charAt(0)));

        if (choice >= 0 && choice < courseGradesArrayList.size()) {
            System.out.println("Add good block or bad?  (g for good, anything else for bad):");
            if (keyboard.next().charAt(0) == 'g') courseGradesArrayList.get(choice).addBlock(keyboard);
            else courseGradesArrayList.get(choice).addBadBlock(keyboard);
        } else System.out.println("The number you typed is not in the list above");
    }

    /**
     * Verify whether every score is correct.
     */
    public void verifyChains() {
        IntStream.range(0, this.courseGradesArrayList.size()).forEach(value -> {
            System.out.println(courseGradesArrayList.get(value).verifyChain() ? "Chain for " + courseGradesArrayList.get(value).getCourseName() + " is verified" : "Chain for " + courseGradesArrayList.get(value).getCourseName() + " is not verified");
        });
    }

    /**
     * Remove the first broken block.
     */
    public void fixChain(Scanner keyboard) {
        IntStream.range(0, this.courseGradesArrayList.size()).forEach(value -> System.out.println(value + " " + courseGradesArrayList.get(value).getCourseName()));

        int choice = (Character.getNumericValue(keyboard.next().charAt(0)));

        if (choice >= 0 && choice < courseGradesArrayList.size()) courseGradesArrayList.get(choice).fixChain();

        else System.out.println("You did not type the number above");
    }
}
