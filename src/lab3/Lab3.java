package lab3;

import java.util.Scanner;

public class Lab3 {

    public static void main(String[] args) {

        System.out.println("Enter a string: ");
        String word = new Scanner(System.in).nextLine().replaceAll("\\s+", "").toLowerCase();
        System.out.println(isPalindrome(word) ? word + " is a palindrome" : word + " is not a palindrome");
    }

    private static boolean isPalindrome(String word) {
        if (word.length() == 0 || word.length() == 1) {
            return true;
        } else if (word.charAt(0) != word.charAt(word.length() - 1)) {
            return false;
        } else {
            return isPalindrome(word.substring(1, word.length() - 1));
        }
    }
}
