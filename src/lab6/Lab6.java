package lab6;

import java.util.LinkedList;
import java.util.Scanner;

public class Lab6 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        boolean ifBalanced = true;

        LinkedList<Character> linkedList = new LinkedList<>();

        System.out.println("Enter an expression");

        String toBeParsed = scanner.nextLine();


        char[] cArray = toBeParsed.toCharArray();

        for (char c : cArray) {

            if (c == '{' || c == '(') {
                linkedList.addFirst(c);
            }

            if (c == '}') {
                if (linkedList.isEmpty()) {
                    ifBalanced = false;
                } else if (linkedList.getFirst() == '{') {
                    linkedList.removeFirst();
                } else {
                    ifBalanced = false;
                }
            }

            if (c == ')') {
                if (linkedList.isEmpty()) {
                    ifBalanced = false;
                } else if (linkedList.getFirst() == '(') {
                    linkedList.removeFirst();
                } else {
                    ifBalanced = false;
                }
            }
        }

        if (!linkedList.isEmpty()) ifBalanced = false;

        System.out.println(ifBalanced ? "The expression is balanced" : "The expression is NOT balanced");

    }
}
