package lab5;

import java.util.Random;
import java.util.Scanner;

public class BlockChain {
    private Block head = new Block(); // start the chain with the Genesis block
    private Block tail = head;
    private String courseName = "NotEntered";

    public BlockChain(String courseName) {
        this.courseName = new String(courseName);
    }

    public void printBlockChain() {
        System.out.println("Chain for " + courseName);
        Block block = head;
        do {
            System.out.println(block);
            block = block.next();
        } while (block != null);
    }

    public boolean verifyChain() {
        Block block = head;
        while (block.next() != null) {
            if (!block.next().isEqual(block)) return false;
            block = block.next();
        }
        return true;
    }

    public void addBlock(Scanner keyboard) {
        Block block = new Block();
        block.addInfoToBlock(keyboard, tail.getCurrentHash());
        tail.updateNext(block);
        tail = block;
    }

    public void addBadBlock(Scanner keyboard) {
        Random random = new Random();
        Block newOne = new Block();
        if (newOne.addInfoToBlock(keyboard, random.nextFloat())) {
            tail.updateNext(newOne);
            tail = newOne;
        }

    }
}
